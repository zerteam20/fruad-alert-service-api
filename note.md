# Fraud Alert Service (FAS) API Contract

## Version: 2
> Version Date Revision Notes
> 1.0.0 August 2020 First version
> 2.0.0 Feb 2021 Second version

> Customer’s phone number is sent to 3rd party for verification and if the phone number is valid then customer info is stored into Craft’s

### Resource
```
/customers
/customers/customerId
```
### Find/retrieve Customer
> customerId(UriParams)

### Data source       
```json
{
    "customerId": 1,
    "firstName": "Jhon",
    "lastName": "Smith",
    "email": "smith.j@gmail.com",
    "PhoneNumber": 2024689999
}
```
### Validation in 3rd party [numverify](https://numverify.com/)
> 3rd party call--->phoneNumber?     is valid yes: CustomerInf Modern DB
```json
{
  "valid":true,
  "number":"12025799452",
  "local_format":"2025799452",
  "international_format":"+12025799452",
  "country_prefix":"+1",
  "country_code":"US",
  "country_name":"United States of America",
  "location":"Wshngtnzn1",
  "carrier":"Cellco Partnership (Verizon Wireless)",
  "line_type":"mobile"
}
```
### Target /Craft Modern Database/ (Craft Soft Fraud Alert Service data base)
> DB Type: MongoDB
> database name: craft-modern-db
> collection name: customers
```json
{
    "_id":"id",
    "customerId": 1,
    "firstName": "Jhon",
    "lastName": "Smith",
    "email": "smith.j@gmail.com",
    "PhoneNumber": 2024689999
}
```
 

### request/response
> Data type: json
> Content-Type=application/json

### Environment
> Development
```
dev.properties
```
> QA
```
qa.properties
```
> Production
``` 
prod.properties
```

### Authentication
Craft Soft ESB currently uses BASIC user authentication in the process layer.
userName: required
password: required

### API Policy
Craft Soft ESB uses Rate limiting and client ID enforcement in system layer.

login-   API phone valid or not



   